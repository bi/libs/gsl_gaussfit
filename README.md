# gsl_gaussfit

### A library for fitting Gauss functions to data

This library is based on the Gnu Scientific library (GSL) v 1.15.

Look at the doc directory for the documentation.

The source code can be found at [https://gitlab.cern.ch/bi/libs/gsl_gaussfit](https://gitlab.cern.ch/bi/libs/gsl_gaussfit)

The documentation can be found at [https://be-bi-pm-eos.web.cern.ch/be-bi-pm-eos/docs/gsl_gaussfit](https://be-bi-pm-eos.web.cern.ch/be-bi-pm-eos/docs/gsl_gaussfit)

### Install

clone or download the repository and run make

	git clone https://gitlab.cern.ch/bi/libs/gsl_gaussfit
	cd gsl_gaussfit
	make

	sudo make install (installs in /usr/local)


### Usage
Example

```cpp

#include <gauss-fit.h>


// Fits a G5 Gauss function (default) trough the given data points
int do_fit(double *in_x, double *in_y, double *in_e, uint in_size) {

	gauss_fit::GaussFit g5fit= gauss_fit::GaussFit(in_x, in_y, in_e, in_size);

	g5fit.fit(NULL);

	if (g5fit.status >= 0) {
		in_par[F_OFFSET]   = g5fit.get_coefficient(NULL, gauss_fit::COEFF_OFFSET);
		in_par[F_SLOPE]    = g5fit.get_coefficient(NULL, gauss_fit::COEFF_SLOPE);
		in_par[F_POSITION] = g5fit.get_coefficient(NULL, gauss_fit::COEFF_AMPLITUDE);
		in_par[F_SIGMA]    = g5fit.get_coefficient(NULL, gauss_fit::COEFF_CENTRE);
		in_par[F_AMPLITUDE]= g5fit.get_coefficient(NULL, gauss_fit::COEFF_SIGMA);
		return 1;
	} else {
		in_par[F_OFFSET]   = -1.0;
		in_par[F_SLOPE]    = -1.0;
		in_par[F_POSITION] = -1.0;
		in_par[F_SIGMA]    = -1.0;
		in_par[F_AMPLITUDE]= -1.0;
		return 0;
	}

	return 0;
}

	// Calculate the value of a G5 Gaussian defined by the `par` coefficients.
	void gauss_function(double *x, double *y, const double *par, int size) {
		int i;

		for (i = 0; i < size; i++) {
			y[i] = gauss_fit::GaussFit::g5_func(x[i], par[F_OFFSET], par[F_SLOPE],
					par[F_AMPLITUDE], par[F_POSITION], par[F_SIGMA]);
		}
	}

```