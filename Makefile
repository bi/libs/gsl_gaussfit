LIB_NAME = libgauss-fit
VERSION = 1.1.1
CPU = L867

BUILDDIR = build
SRCDIR = src
DOCDIR = doc

BDI_BIN_DIR = /user/bdisoft/operational/lib/$(CPU)/gsl_gaussfit/$(VERSION)
BDI_SRC_DIR = /user/bdisoft/development/lib/src/gsl_gaussfit/$(VERSION)
PMDOC_DIR = /eos/project-b/be-bi-pm/www/docs/Library

ARFLAGS= rc

CPPFLAGS= -g -fPIC -D LIB_VERSION='"$(VERSION)"'
LDLIBS= -lgsl -lgslcblas -lm

all: $(BUILDDIR) $(BUILDDIR)/$(LIB_NAME).so $(BUILDDIR)/$(LIB_NAME).a test

$(BUILDDIR):
	@ mkdir -p $(BUILDDIR)

$(DOCDIR):
	@ mkdir -p $(DOCDIR)

$(BUILDDIR)/$(LIB_NAME).o: $(SRCDIR)/$(LIB_NAME).cpp
	$(COMPILE.cpp) $(OUTPUT_OPTION) $<

$(BUILDDIR)/$(LIB_NAME).a: $(BUILDDIR)/$(LIB_NAME).o
	ar rucs $@ $^

$(BUILDDIR)/$(LIB_NAME).so: $(BUILDDIR)/$(LIB_NAME).o
	$(CXX) -shared $(LDFLAGS) $(TARGET_ARCH) -Wl,-soname,`basename $@` -o $@.$(VERSION) $^ $(LOADLIBES) $(LDLIBS)
	ln -sf `basename $@`.$(VERSION) $@

test:
	make -C test clean all
	
pymod:
	make -C PyGaussFit clean all

docs: $(DOCDIR)
	rm -rf doc/html/* doc/latex/*
	(export PROJECT_NUMBER=$(VERSION); doxygen doc.cfg)
	@echo
	@echo "#######################################################"
	@echo "If you are using CC7 the md parsing is wrong use >= CC8"
	@echo "like a VM in openstack eb-opsk-cs8"
	@echo "#######################################################"
	
pdf:
	make -C doc/latex
		
clean:
	rm -rf build
	make -C test clean
	make -C PyGaussFit clean
	
install:
	cp -d build/libgauss-fit.so* /usr/local/lib64
	cp src/gauss-fit.h /usr/local/include
	
bdi:
	mkdir -p $(BDI_BIN_DIR)/lib
	mkdir -p $(BDI_BIN_DIR)/include
	mkdir -p $(BDI_BIN_DIR)/doc
	cp $(BUILDDIR)/libgauss-fit.a $(BDI_BIN_DIR)/lib
	cp $(SRCDIR)/gauss-fit.h $(BDI_BIN_DIR)/include
	cp README.md $(BDI_BIN_DIR)
	cp -r doc/html/* $(BDI_BIN_DIR)/doc
	mkdir -p $(BDI_SRC_DIR)
	rsync -r -l --exclude='.*' ./ $(BDI_SRC_DIR)

gittag:
	@test -z "`git status --porcelain -z`" || \
	(echo -e "\n\033[0;31m### Git working tree not clean, use git commit [-a] ###\033[0m\n"; git status ; exit 1)
	git push
	git tag -a -m "Release of version v$(VERSION)" v$(VERSION)
	git push --tags

pmdocs:
	mkdir -p $(PMDOC_DIR)/gsl_gaussfit-docs-$(VERSION)
	rsync -r -l doc/html/ $(PMDOC_DIR)/gsl_gaussfit-docs-$(VERSION)/

.PHONY: test pymod doc doxygen pmdoc pdf clean install
	