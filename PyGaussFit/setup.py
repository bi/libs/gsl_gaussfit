from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import re

version = "0.1"
with open("../doc.cfg", "r") as fp:
    for line in fp.readlines():
        m = re.match("PROJECT_NUMBER\s*=\s*([0-9.]*)", line)
        if m:
            version = m[1]
            break

ext_modules= cythonize([Extension("PyGaussFit", ["PyGaussFit.pyx"],
                             libraries=['gauss-fit', 'gsl', 'gslcblas', 'm'],
                             library_dirs=["../"])
                        ])

setup(author="Enrico Bravin",
      author_email='enrico.bravin@cern.ch',
      name='py_gauss_fit',
      description='Binding for the libgauss-fit library based on gsl.',
      license='MIT license',
      ext_modules=ext_modules,
      version=version
      )