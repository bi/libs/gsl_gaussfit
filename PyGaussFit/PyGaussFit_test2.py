import PyGaussFit as gf
import numpy as np
import matplotlib.pyplot as plt

def gauss(x, offset, slope, amplitude, centre, sigma):
    return offset + slope * x + amplitude * np.exp(-0.5 * ((x - centre) / sigma) ** 2)

def asym_gauss(x, offset, slope, amplitude, centre, sigma_l, sigma_r):
    sigma = np.zeros(len(x)) + sigma_l
    sigma[np.where(x >= centre)] = sigma_r
    return offset + slope * x + amplitude * np.exp(-0.5 * ((x - centre) / sigma) ** 2)

def double_gauss(x, offset, slope, amplitude_1, amplitude_2, centre, sigma_1, sigma_2):
    sigma = np.zeros(len(x)) + sigma_l
    sigma[np.where(x >= 0)] = sigma_r
    return (offset + slope * x + amplitude_1 * np.exp(-0.5 * ((x - centre) / sigma_1) ** 2) +
            amplitude_2 * np.exp(-0.5 * ((x - centre) / sigma_2) ** 2))

print(gf.__version__)

x_min = -10.0
x_max = 10.0
n_points = 300
M = 5
n_cycles = M*M

sigma = np.abs(np.random.normal(1.0, 0.4, n_cycles))
centre = np.random.normal(0.0, 4.0, n_cycles)
amplitude = np.abs(np.random.normal(5000.0, 2000, n_cycles))
offset = np.random.normal(0.0, 500.0, n_cycles)
slope = np.random.normal(0.0, 100.0, n_cycles)

x = np.linspace(x_min, x_max, n_points)

sigma_noise = amplitude * 0.05  # 5% white noise

fig, axes = plt.subplots(M, M, figsize=(25,12.5))

for n in range(n_cycles):
    noise = np.random.normal(0, sigma_noise[n], n_points)
    w = np.zeros(n_points) + 1.0 / sigma_noise[n]

    y = gauss(x, offset[n], slope[n], amplitude[n], centre[n], sigma[n]) + noise
    f = gf.PyGaussFit(x, y, w)
    f.set_verbose(False)
    #start = np.array([0, 0, amplitude[n] / 5., 0, sigma[n] / 2.])
    start = f.get_init_values(gf.GFIT_OMACS, 0.2)
    ymin_n = np.argmin(y)
    ymax_n = np.argmax(y)
    start2 = np.array([0.0, 0.0, y[ymax_n]-y[ymin_n], x[ymax_n], 1.0])
    result = f.fit(start2, gf.GFIT_OMACS, 1e-4, 0.0)
    k = result['result']
    k[4] = np.abs(k[4])
    error = (k[4] - sigma[n]) / sigma[n] * 100
    print(f"{int(n/M): 2d}, {n%M: 2d}  {amplitude[n]: 6.0f} {centre[n]: 5.1f} {sigma[n]: 6.3f}  {k[2]: 6.0f} {k[3]: 5.1f} {k[4]: 6.3f}    start {start[0]: 5.0f} {start[1]: 5.0f} {start[2]: 6.0f} {start[3]: 5.1f} {start[4]: 6.3f}  error {error: 5.1f}")
    #f.print_results()

    ns = int(n_points/10.0)
    ys = np.mean(y[:ns])
    ye = np.mean(y[-ns:])
    xs = np.mean(x[:ns])
    xe = np.mean(x[-ns:])
    m = (ye-ys) / (xe - xs)
    o = ys - m * xs
    y2 = y - x*m - o
    amp = np.amax(y2) - np.amin(y2)
    y3 = np.array(y2)
    y2[y2<=(amp*0.2)] = 0.0
    sel = y2>0.0
    norm = np.sum(y2)
    mean = np.sum(y2*x)/norm
    rms = np.sqrt(np.sum(y2*(x**2))/norm - mean**2)
    print(f"True   {offset[n]: 5.0f} {slope[n]: 5.0f} {amplitude[n]: 6.0f} {centre[n]: 5.1f} {sigma[n]: 6.3f}")
    print(f"Python {o: 5.0f} {m: 5.0f} {amp: 6.0f} {mean: 5.1f} {rms: 6.3f}")
    print(f"c++    {start[0]: 5.0f} {start[1]: 5.0f} {start[2]: 6.0f} {start[3]: 5.1f} {start[4]: 6.3f}")
    print(f"Simpl  {start2[0]: 5.0f} {start2[1]: 5.0f} {start2[2]: 6.0f} {start2[3]: 5.1f} {start2[4]: 6.3f}")

    ax = axes[int(n/M)][n%M]
    ax.plot(x, y, 'b.', ms=1)
    ax.plot(x, y2, 'k.', ms=0.5)
    #ax.plot(x[sel], y[sel], 'g.', ms=2)
    ax.plot(x, y3, 'g.', ms=2)
    ax.plot(x, gauss(x, k[0], k[1], k[2], k[3], k[4]), 'r')
    ax.set_title(f"{sigma[n]:.3f}")

fig.tight_layout()
plt.show()

