import PyGaussFit as gf
import numpy as np
import matplotlib.pyplot as plt

def gauss(x, offset, slope, amplitude, centre, sigma):
    return offset + slope*x + amplitude*np.exp(-0.5*((x-centre)/sigma)**2)

def asym_gauss(x, offset, slope, amplitude, centre, sigma_l, sigma_r):
    sigma= np.zeros(len(x)) + sigma_l
    sigma[np.where(x>=centre)]= sigma_r
    return offset + slope*x + amplitude*np.exp(-0.5*((x-centre)/sigma)**2)

def double_gauss(x, offset, slope, amplitude_1, amplitude_2, centre, sigma_1, sigma_2):
    sigma= np.zeros(len(x)) + sigma_l
    sigma[np.where(x>=0)]= sigma_r
    return (offset + slope*x + amplitude_1*np.exp(-0.5*((x-centre)/sigma_1)**2) +
            amplitude_2*np.exp(-0.5*((x-centre)/sigma_2)**2))

print(gf.__version__)

sigma= 2.0
centre= -1.0
amplitude= 5000.0
offset= 1000
slope= 100.0

sigma_noise= amplitude*0.05 # 5% white noise

x_min= -10.0
x_max= 10.0
n_points= 100

x= np.linspace(x_min, x_max, n_points)

noise= np.random.normal(0, sigma_noise, n_points)
w= np.zeros(n_points)+1.0/sigma_noise

y= gauss(x, offset, slope, amplitude, centre, sigma) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([0, 0, amplitude/5., 0, sigma/2.])
result= f.fit(start, gf.GFIT_OMACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, gauss(x, k[0], k[1], k[2], k[3], k[4]), 'r')

y= gauss(x, offset, 0, amplitude, centre, sigma) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([0, amplitude/5., 0, sigma/2.])
result= f.fit(start, gf.GFIT_OACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, gauss(x, k[0], 0, k[1], k[2], k[3]), 'r')

y= gauss(x, 0, 0, amplitude, centre, sigma) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([amplitude/5., 0, sigma/2.])
result= f.fit(start, gf.GFIT_ACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, gauss(x, 0, 0, k[0], k[1], k[2]), 'r')


#plt.show()

print('\n\n')

slope= 20.0
offset= 100.0
centre= -1.0
amplitude= 5000.0
sigma_l= 1.1
sigma_r= 2.3
sigma_noise= amplitude*0.05 # 5% white noise

noise= np.random.normal(0, sigma_noise, n_points)
w= np.zeros(n_points)+1.0/sigma_noise

y= asym_gauss(x, offset, slope, amplitude, centre, sigma_l, sigma_r) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([0, 0, amplitude*1.3, 0, sigma_l*0.7, sigma_r*1.3])
result= f.fit(None, gf.GFIT_ASYM_OMACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, asym_gauss(x, k[0], k[1], k[2], k[3], k[4], k[5]), 'r')

y= asym_gauss(x, offset, 0, amplitude, centre, sigma_l, sigma_r) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([0, amplitude*1.3, 0, sigma_l*0.7, sigma_r*1.3])
result= f.fit(None, gf.GFIT_ASYM_OACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, asym_gauss(x, k[0], 0, k[1], k[2], k[3], k[4]), 'r')

y= asym_gauss(x, 0, 0, amplitude, centre, sigma_l, sigma_r) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([amplitude*1.3, 0, sigma_l*0.7, sigma_r*1.3])
result= f.fit(None, gf.GFIT_ASYM_ACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, asym_gauss(x, 0, 0, k[0], k[1], k[2], k[3]), 'r')

#plt.show()

print('\n\n')

slope= 10.0
offset= 200.0
centre= 0.3
amplitude_1= 4000.0
amplitude_2= 4000.0
sigma_1= 0.95
sigma_2= 1.8
noise_ampl = amplitude_1*0.01
noise= np.random.normal(0, noise_ampl, n_points)
w= np.zeros(n_points)+1.0/noise_ampl

y= double_gauss(x, offset, slope, amplitude_1, amplitude_2, centre, sigma_1, sigma_2) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([0, 0, amplitude_1*1.3, amplitude_2*0.7 ,0, sigma_1*0.7, sigma_2*1.3])
result= f.fit(None, gf.GFIT_DG_OMACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, double_gauss(x, k[0], k[1], k[2], k[3], k[4], k[5], k[6]), 'r')
plt.plot(x, gauss(x, k[0], k[1], k[2], k[4], k[5]), 'k')
plt.plot(x, gauss(x, k[0], k[1], k[3], k[4], k[6]), 'k')

start= np.array([0, 0, amplitude_1, 0, sigma_1])
result= f.fit(None, gf.GFIT_OMACS, 1e-4, 0.0)
f.print_results()
k = result['result']
plt.plot(x, gauss(x, k[0], k[1], k[2], k[3], k[4]), 'b')

y= double_gauss(x, offset, 0, amplitude_1, amplitude_2, centre, sigma_1, sigma_2) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([0, amplitude_1*1.3, amplitude_2*0.7 ,0, sigma_1*0.7, sigma_2*1.3])
result= f.fit(None, gf.GFIT_DG_OACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, double_gauss(x, k[0], 0, k[1], k[2], k[3], k[4], k[5]), 'r')

y= double_gauss(x, 0, 0, amplitude_1, amplitude_2, centre, sigma_1, sigma_2) + noise
f= gf.PyGaussFit(x, y, w)
f.set_verbose(True)
start= np.array([amplitude_1*1.3, amplitude_2*0.7 ,0, sigma_1*0.7, sigma_2*1.3])
result= f.fit(None, gf.GFIT_DG_ACS, 1e-4, 0.0)
f.print_results()
plt.figure()
plt.plot(x, y, 'bo')
k = result['result']
plt.plot(x, double_gauss(x, 0, 0, k[0], k[1], k[2], k[3], k[4]), 'r')

plt.show()

