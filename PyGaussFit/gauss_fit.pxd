from libcpp cimport bool

# Decalre the class with cdef
cdef extern from "../gauss-fit.h" namespace "gauss_fit":
    char *__GAUSSFIT_VERSION__

    ctypedef enum coefficient_type_t:
        COEFF_OFFSET
        COEFF_SLOPE
        COEFF_AMPLITUDE
        COEFF_AMPLITUDE_1
        COEFF_AMPLITUDE_2
        COEFF_CENTRE
        COEFF_SIGMA
        COEFF_SIGMA_L
        COEFF_SIGMA_R
        COEFF_SIGMA_1
        COEFF_SIGMA_2
        COEFFICIENTS_MAX_SIZE

    ctypedef enum gfit_type_t:
        GAUSSFIT_ACS
        GAUSSFIT_OACS
        GAUSSFIT_OMACS
        GAUSSFIT_ASYM_ACS
        GAUSSFIT_ASYM_OACS
        GAUSSFIT_ASYM_OMACS
        GAUSSFIT_DG_ACS
        GAUSSFIT_DG_OACS
        GAUSSFIT_DG_OMACS

#    GaussFit(double *x, double *y, double *w, size_t nb_of_points);
    cdef cppclass GaussFit:
        GaussFit(double * , double * , double * , size_t) except +
        double result[10];
        double error[10];
        double chisq;
        int iterations;
        double dof;
        int status;
        size_t _n;
        size_t _p;

        int set_data(double * x, double * y, double * w, size_t nb_of_points)

        int fit(double * init_values, gfit_type_t fit_type, double abs_tol,
                double rel_tol)

        bool get_init_values(double *init_values, gfit_type_t fit_type, double thr)

        double get_coefficient(double *coefficients, coefficient_type_t which)

        @staticmethod
        double g2_func(double x, double mu, double sigma)
        @staticmethod
        double g3_func(double x, double amp, double mu, double sigma)
        @staticmethod
        double g4_func(double x, double offset, double amp, double mu, double sigma)
        @staticmethod
        double g5_func(double x, double offset, double slope, double amp, double mu, double sigma)

        @staticmethod
        double dg_g5_func(double x, double amp1, double amp2, double mu, double sigma1, double sigma2)
        @staticmethod
        double dg_g6_func(double x, double offset, double amp1, double amp2, double mu, double sigma1, double sigma2)
        @staticmethod
        double dg_g7_func(double x, double offset, double slope, double amp1, double amp2, double mu, double sigma1, double sigma2)

        @staticmethod
        double asym_g3_func(double x, double mu, double sigma_l,
            double sigma_r)
        @staticmethod
        double asym_g4_func(double x, double amp, double mu, double sigma_l,
            double sigma_r)
        @staticmethod
        double asym_g5_func(double x, double offset, double amp, double mu,
            double sigma_l, double sigma_r)
        @staticmethod
        double asym_g6_func(double x, double offset, double slope,
            double amp, double mu, double sigma_l, double sigma_r)

        void set_verbose(bool verbose)
        bool get_verbose()

        void print_results()

        size_t get_n()
        size_t get_p()
        gfit_type_t get_fit_type()


