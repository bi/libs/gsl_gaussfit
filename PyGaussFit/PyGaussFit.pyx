# distutils: language = c++

from gauss_fit cimport *
import numpy as np

__version__ = __GAUSSFIT_VERSION__.decode()

GFIT_OFFSET = COEFF_OFFSET
GFIT_SLOPE = COEFF_SLOPE
GFIT_AMPLITUDE = COEFF_AMPLITUDE
GFIT_AMPLITUDE_1 = COEFF_AMPLITUDE_1
GFIT_AMPLITUDE_2 = COEFF_AMPLITUDE_2
GFIT_CENTRE = COEFF_CENTRE
GFIT_SIGMA = COEFF_SIGMA
GFIT_SIGMA_L = COEFF_SIGMA_L
GFIT_SIGMA_R = COEFF_SIGMA_R
GFIT_SIGMA_1 = COEFF_SIGMA_1
GFIT_SIGMA_2 = COEFF_SIGMA_2

GFIT_ACS = GAUSSFIT_ACS
GFIT_OACS = GAUSSFIT_OACS
GFIT_OMACS = GAUSSFIT_OMACS
GFIT_ASYM_ACS = GAUSSFIT_ASYM_ACS
GFIT_ASYM_OACS = GAUSSFIT_ASYM_OACS
GFIT_ASYM_OMACS = GAUSSFIT_ASYM_OMACS
GFIT_DG_ACS = GAUSSFIT_DG_ACS
GFIT_DG_OACS = GAUSSFIT_DG_OACS
GFIT_DG_OMACS = GAUSSFIT_DG_OMACS


cdef class PyGaussFit:
    cdef GaussFit * ptr
    def __cinit__(self, double[:] x, double[:] y, double[:] w):
        n = len(x)
        self.ptr = new GaussFit(& x[0], & y[0], & w[0], n)

    def __dealloc__(self):
        del self.ptr

    def fit(self, double[:] init_values, gfit_type_t fit_type=GAUSSFIT_OMACS, double abs_tol=1e-4, double rel_tol=1e-4):
        if init_values is None:
            self.ptr.fit(NULL, fit_type, abs_tol, rel_tol)
        else:
            self.ptr.fit(& init_values[0], fit_type, abs_tol, rel_tol)

        p = int(self.ptr.get_p())
        result = [0.0] * p
        error = [0.0] * p
        for i in range(p):
            result[i] = self.ptr.result[i]
            error[i] = self.ptr.error[i]

        return {'result': result, 'error': error,
                'chisq': self.ptr.chisq, 'iterations': self.ptr.iterations,
                'dof': self.ptr.dof, 'status': self.ptr.status}

    def get_init_values(self, gfit_type_t fit_type, double thr=0.2):
        cdef double[::1] init_values = np.zeros(10)
        res = self.ptr.get_init_values(&init_values[0], fit_type, thr)
        return init_values

    def set_data(self, double[:] x, double[:] y, double[:] w):
        n = len(x)
        self.ptr.set_data(& x[0], & y[0], & w[0], n)

    def get_coefficient(self, double[:] coefficients, coefficient_type_t which):
        if coefficients is None:
            return self.ptr.get_coefficient(NULL, which)
        else:
            return self.ptr.get_coefficient(& coefficients[0], which)

    @staticmethod
    def g2_func(double x, double mu, double sigma):
        return GaussFit.g2_func(x, mu, sigma)

    @staticmethod
    def g3_func(double x, double amp, double mu, double sigma):
        return GaussFit.g3_func(x, amp, mu, sigma)

    @staticmethod
    def g4_func(double x, double offset, double amp, double mu, double sigma):
        return GaussFit.g4_func(x, offset, amp, mu, sigma)

    @staticmethod
    def g5_func(double x, double offset, double slope, double amp, double mu, double sigma):
        return GaussFit.g5_func(x, offset, slope, amp, mu, sigma)

    @staticmethod
    def dg_g5_func(double x, double amp_1, double amp_2, double mu, double sigma_1, double sigma_2):
        return GaussFit.dg_g5_func(x, amp_1, amp_2, mu, sigma_1, sigma_2)

    @staticmethod
    def dg_g6_func(double x, double offset, double amp_1, double amp_2, double mu, double sigma_1, double sigma_2):
        return GaussFit.dg_g6_func(x, offset, amp_1, amp_2, mu, sigma_1, sigma_2)

    @staticmethod
    def dg_g7_func(double x, double offset, double slope, double amp_1, double amp_2, double mu, double sigma_1, double sigma_2):
        return GaussFit.dg_g7_func(x, offset, slope, amp_1, amp_2, mu, sigma_1, sigma_2)

    @staticmethod
    def asym_g3_func(double x, double mu, double sigma_l, double sigma_r):
        return GaussFit.asym_g3_func(x, mu, sigma_l, sigma_r)

    @staticmethod
    def asym_g4_func(double x, double amp, double mu, double sigma_l, double sigma_r):
        return GaussFit.asym_g4_func(x, amp, mu, sigma_l, sigma_r)

    @staticmethod
    def asym_g5_func(double x, double offset, double amp, double mu, double sigma_l, double sigma_r):
        return GaussFit.asym_g5_func(x, offset, amp, mu, sigma_l, sigma_r)

    @staticmethod
    def asym_g6_func(double x, double offset, double slope, double amp, double mu, double sigma_l, double sigma_r):
        return GaussFit.asym_g6_func(x, offset, slope, amp, mu, sigma_l, sigma_r)

    def set_verbose(self, bool verbose):
        self.ptr.set_verbose(verbose)

    def get_verbose(self):
        return self.ptr.get_verbose()

    def print_results(self):
        self.ptr.print_results()

    def get_status(self):
        return self.ptr.status

    def get_n(self):
        return self.ptr.get_n()

    def get_p(self):
        return self.ptr.get_p()

    def get_fit_type(self):
        return self.ptr.get_fit_type()
