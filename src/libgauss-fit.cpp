#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "gauss-fit.h"

namespace gauss_fit {

const char *__GAUSSFIT_VERSION__= LIB_VERSION;
bool __gsl_eh_initialized = false;

#define MAX_ITERATIONS 500
// This will turn off range check in the vector operations (vector_get). In our case should be quite safe
#define GSL_RANGE_CHECK_OFF 1

double vector_mean(const gsl_vector *v, size_t start, size_t n) {
	int i;
	double sum = 0.0;

	if (n <= 0)
		n = v->size;

	for (i=0;i<n;i++) {
		sum+= gsl_vector_get(v, i+start);
	}

	return sum/n;
}

double vector_rms(const gsl_vector *v, size_t start, size_t n) {
	int i;
	double t;
	double m1 = 0.0;
	double m2 = 0.0;

	if (n <= 0)
		n = v->size;

	for (i=0;i<n;i++) {
		t = gsl_vector_get(v, i+start);
		m1+= t;
		m2+= t*t;
	}
	m1 = m1 / n;
	m2 = m2 / n;

	return sqrt(m2 - m1*m1);
}


GaussFit::GaussFit(const double *x, const double *y, const double *w, size_t nb_of_points) :
		_n(nb_of_points), _p(0), dof(0), _vx(NULL), _vy(NULL), _vw(NULL), _fit_type(
				GAUSSFIT_ACS), _verbose(false), status(0), chisq(0), iterations(
				0) {

    if(!__gsl_eh_initialized) {
        // The default behavior of gsl is to abort the program on error! With this it does nothng
        gsl_set_error_handler_off();
        __gsl_eh_initialized = true;
    }

	set_data(x, y, w, nb_of_points);
}

GaussFit::~GaussFit() {
	_n = 0;
	gsl_vector_free(_vx);
	gsl_vector_free(_vy);
	gsl_vector_free(_vw);

}

void GaussFit::set_data(const double *x, const double *y, const double *w, size_t nb_of_points) {
	// We recreate the vectors only if they have a different size
	if (nb_of_points != _n) {
		if (_vx != NULL) {
			gsl_vector_free(_vx);
			_vx = NULL;
		}
		if (_vy != NULL) {
			gsl_vector_free(_vy);
			_vy = NULL;
		}
		if (_vw != NULL) {
			gsl_vector_free(_vw);
			_vw = NULL;
		}
	}

	_n = nb_of_points;

	if (_vx == NULL)
		_vx = gsl_vector_alloc(_n);
	if (_vy == NULL)
		_vy = gsl_vector_alloc(_n);
	if (_vw == NULL)
		_vw = gsl_vector_alloc(_n);

	// We use the vector views just to copy the data, so we can safely cast to const double*
	gsl_vector_view vv_x = gsl_vector_view_array(const_cast<double*>(x), nb_of_points);
	gsl_vector_view vv_y = gsl_vector_view_array(const_cast<double*>(y), nb_of_points);

	gsl_vector_memcpy(_vx, &vv_x.vector);
	gsl_vector_memcpy(_vy, &vv_y.vector);

	if (w != NULL) {
		// We use the vector views just to copy the data, so we can safely cast to const double*
		gsl_vector_view vv_w = gsl_vector_view_array(const_cast<double*>(w), nb_of_points);
		gsl_vector_memcpy(_vw, &vv_w.vector);
	} else {
		gsl_vector_set_all(_vw, 1.0);
	}

	for (int i = 0; i < 10; i++) {
		result[i] = 0;
		error[i] = 0;
	}
}

double GaussFit::get_coefficient(double *coefficients,
		coefficient_type_t which) {
	double O, M, A1, A2, C, S1, S2;

	double *ptr;
	if (coefficients != NULL)
		ptr = coefficients;
	else
		ptr = result;
	gsl_vector_view coeffs = gsl_vector_view_array(ptr, COEFFICIENTS_MAX_SIZE);

	if (_fit_type < GAUSSFIT_ASYM_ACS) {
		get_coeffs(&coeffs.vector, &O, &M, &A1, &C, &S1);
		S2 = 0;
	} else if (_fit_type < GAUSSFIT_DG_OMACS) {
		get_asym_coeffs(&coeffs.vector, &O, &M, &A1, &C, &S1, &S2);
	} else {
		get_dg_coeffs(&coeffs.vector, &O, &M, &A1, &A2, &C, &S1, &S2);
	}

	switch (which) {
	case COEFF_OFFSET:
		return O;
		break;
	case COEFF_SLOPE:
		return M;
		break;
	case COEFF_AMPLITUDE:
	case COEFF_AMPLITUDE_1:
		return A1;
		break;
	case COEFF_AMPLITUDE_2:
		return A2;
		break;
	case COEFF_CENTRE:
		return C;
		break;
	case COEFF_SIGMA:
	case COEFF_SIGMA_L:
	case COEFF_SIGMA_1:
		return S1;
		break;
	case COEFF_SIGMA_R:
	case COEFF_SIGMA_2:
		return S2;
		break;
	default:
		return 0;
	}

}

inline double GaussFit::g2_func(double x, double mu, double sigma) {
	double k;
	double f;

	k = (x - mu) / sigma;
	f = exp(-0.5 * k * k);

	return (f);
}

inline double GaussFit::g3_func(double x, double amp, double mu, double sigma) {

	return (amp * g2_func(x, mu, sigma));
}

inline double GaussFit::g4_func(double x, double offset, double amp, double mu,
		double sigma) {

	return (offset + g3_func(x, amp, mu, sigma));
}

inline double GaussFit::g5_func(double x, double offset, double slope,
		double amp, double mu, double sigma) {

	return (slope * x + g4_func(x, offset, amp, mu, sigma));
}

inline double GaussFit::dg_g5_func(double x, double amp1, double amp2, double mu,
		double sigma1, double sigma2) {
	return (g3_func(x, amp1, mu, sigma1) + g3_func(x, amp2, mu, sigma2));
}

inline double GaussFit::dg_g6_func(double x, double offset, double amp1,
		double amp2, double mu, double sigma1, double sigma2) {
	return (offset + dg_g5_func(x, amp1, amp2, mu, sigma1, sigma2));
}

inline double GaussFit::dg_g7_func(double x, double offset, double slope,
		double amp1, double amp2, double mu, double sigma1, double sigma2) {
	return (slope * x + dg_g6_func(x, offset, amp1, amp2, mu, sigma1, sigma2));
}

inline double GaussFit::asym_g3_func(double x, double mu, double sigma_l,
		double sigma_r) {
	double sigma;
	double k;
	double f;

	if (x >= mu) {
		sigma = sigma_r;
	} else {
		sigma = sigma_l;
	}

	k = (x - mu) / sigma;
	f = exp(-0.5 * k * k);

	return (f);
}

inline double GaussFit::asym_g4_func(double x, double amp, double mu,
		double sigma_l, double sigma_r) {

	return (amp * asym_g3_func(x, mu, sigma_l, sigma_r));
}

inline double GaussFit::asym_g5_func(double x, double offset, double amp,
		double mu, double sigma_l, double sigma_r) {

	return (offset + asym_g4_func(x, amp, mu, sigma_l, sigma_r));
}

inline double GaussFit::asym_g6_func(double x, double offset, double slope,
		double amp, double mu, double sigma_l, double sigma_r) {

	return (slope * x + asym_g5_func(x, offset, amp, mu, sigma_l, sigma_r));
}

void GaussFit::get_coeffs(const gsl_vector * coeffs, double *O, double *M,
		double *A, double *C, double *S) {
	switch (_fit_type) {
	case GAUSSFIT_ACS:
		*O = 0.0;
		*M = 0.0;
		*A = gsl_vector_get(coeffs, 0);
		*C = gsl_vector_get(coeffs, 1);
		*S = gsl_vector_get(coeffs, 2);
		break;
	case GAUSSFIT_OACS:
		*O = gsl_vector_get(coeffs, 0);
		*M = 0.0;
		*A = gsl_vector_get(coeffs, 1);
		*C = gsl_vector_get(coeffs, 2);
		*S = gsl_vector_get(coeffs, 3);
		break;
	case GAUSSFIT_OMACS:
		*O = gsl_vector_get(coeffs, 0);
		*M = gsl_vector_get(coeffs, 1);
		*A = gsl_vector_get(coeffs, 2);
		*C = gsl_vector_get(coeffs, 3);
		*S = gsl_vector_get(coeffs, 4);
		break;
	default:
		break;
	}

}

void GaussFit::get_asym_coeffs(const gsl_vector * coeffs, double *O, double *M,
		double *A, double *C, double *SL, double *SR) {
	switch (_fit_type) {
	case GAUSSFIT_ASYM_ACS:
		*O = 0.0;
		*M = 0.0;
		*A = gsl_vector_get(coeffs, 0);
		*C = gsl_vector_get(coeffs, 1);
		*SL = gsl_vector_get(coeffs, 2);
		*SR = gsl_vector_get(coeffs, 3);
		break;
	case GAUSSFIT_ASYM_OACS:
		*O = gsl_vector_get(coeffs, 0);
		*M = 0.0;
		*A = gsl_vector_get(coeffs, 1);
		*C = gsl_vector_get(coeffs, 2);
		*SL = gsl_vector_get(coeffs, 3);
		*SR = gsl_vector_get(coeffs, 4);
		break;
	case GAUSSFIT_ASYM_OMACS:
		*O = gsl_vector_get(coeffs, 0);
		*M = gsl_vector_get(coeffs, 1);
		*A = gsl_vector_get(coeffs, 2);
		*C = gsl_vector_get(coeffs, 3);
		*SL = gsl_vector_get(coeffs, 4);
		*SR = gsl_vector_get(coeffs, 5);
		break;
	default:
		break;
	}

}

void GaussFit::get_dg_coeffs(const gsl_vector * coeffs, double *O, double *M,
		double *A1, double *A2, double *C, double *S1, double *S2) {
	switch (_fit_type) {
	case GAUSSFIT_DG_ACS:
		*O = 0.0;
		*M = 0.0;
		*A1 = gsl_vector_get(coeffs, 0);
		*A2 = gsl_vector_get(coeffs, 1);
		*C = gsl_vector_get(coeffs, 2);
		*S1 = gsl_vector_get(coeffs, 3);
		*S2 = gsl_vector_get(coeffs, 4);
		break;
	case GAUSSFIT_DG_OACS:
		*O = gsl_vector_get(coeffs, 0);
		*M = 0.0;
		*A1 = gsl_vector_get(coeffs, 1);
		*A2 = gsl_vector_get(coeffs, 2);
		*C = gsl_vector_get(coeffs, 3);
		*S1 = gsl_vector_get(coeffs, 4);
		*S2 = gsl_vector_get(coeffs, 5);
		break;
	case GAUSSFIT_DG_OMACS:
		*O = gsl_vector_get(coeffs, 0);
		*M = gsl_vector_get(coeffs, 1);
		*A1 = gsl_vector_get(coeffs, 2);
		*A2 = gsl_vector_get(coeffs, 3);
		*C = gsl_vector_get(coeffs, 4);
		*S1 = gsl_vector_get(coeffs, 5);
		*S2 = gsl_vector_get(coeffs, 6);
		break;
	default:
		break;
	}

}

int GaussFit::gauss_f(const gsl_vector * coeffs, void *data,
		gsl_vector * residuals) {
	GaussFit *gf = (GaussFit*) data;

	double O, M, A, A1, A2, C, S, SL, SR, S1, S2;

	if (gf->_fit_type < GAUSSFIT_ASYM_ACS) {
		gf->get_coeffs(coeffs, &O, &M, &A, &C, &S);

		for (size_t i = 0; i < gf->_n; i++) {
			double xi = gsl_vector_get(gf->_vx, i);
			double yi = gsl_vector_get(gf->_vy, i);
			double wi = gsl_vector_get(gf->_vw, i);
			double Yi = g5_func(xi, O, M, A, C, S);
			gsl_vector_set(residuals, i, (Yi - yi) * wi);
		}
	} else if (gf->_fit_type < GAUSSFIT_DG_ACS) {
		gf->get_asym_coeffs(coeffs, &O, &M, &A, &C, &SL, &SR);

		for (size_t i = 0; i < gf->_n; i++) {
			double xi = gsl_vector_get(gf->_vx, i);
			double yi = gsl_vector_get(gf->_vy, i);
			double wi = gsl_vector_get(gf->_vw, i);
			double Yi = asym_g6_func(xi, O, M, A, C, SL, SR);
			gsl_vector_set(residuals, i, (Yi - yi) * wi);
		}

	} else {
		gf->get_dg_coeffs(coeffs, &O, &M, &A1, &A2, &C, &S1, &S2);

		for (size_t i = 0; i < gf->_n; i++) {
			double xi = gsl_vector_get(gf->_vx, i);
			double yi = gsl_vector_get(gf->_vy, i);
			double wi = gsl_vector_get(gf->_vw, i);
			double Yi = dg_g7_func(xi, O, M, A1, A2, C, S1, S2);
			gsl_vector_set(residuals, i, (Yi - yi) * wi);
		}

	}

	return GSL_SUCCESS;
}

int GaussFit::gauss_df(const gsl_vector * coeffs, void *data,
		gsl_matrix * jacobian) {
	GaussFit *gf = (GaussFit*) data;

	double O, M, A, A1, A2, C, S, SL, SR, S1, S2;

	if (gf->_fit_type < GAUSSFIT_ASYM_ACS) {
		gf->get_coeffs(coeffs, &O, &M, &A, &C, &S);
	} else if (gf->_fit_type < GAUSSFIT_DG_ACS) {
		gf->get_asym_coeffs(coeffs, &O, &M, &A, &C, &SL, &SR);
	} else {
		gf->get_dg_coeffs(coeffs, &O, &M, &A1, &A2, &C, &S1, &S2);
	}

	for (size_t i = 0; i < gf->_n; i++) {
		/*
		 * f(R_j) -> R_i
		 * f(x_i, O, M, A, C, S) -> y_i
		 *
		 * Jacobian matrix J(i,j) = dfi / dcoff_j
		 *
		 * where fi = (Yi - yi)/w[i],
		 *       Yi = O + x*M + A * exp(-1/2*((x-C)/S)^2)
		 */
		double s, s2, s3, s1_2, s2_2, s1_3, s2_3;
		double xi = gsl_vector_get(gf->_vx, i);
		double d = xi - C;
		double d2 = d * d;
		double g, g1, g2;
		if (gf->_fit_type < GAUSSFIT_ASYM_ACS) {
			s2 = S * S;
			s3 = s2 * S;
			g = g2_func(xi, C, S);
		} else if (gf->_fit_type < GAUSSFIT_DG_ACS) {
			if (d >= 0) {
				s2 = SR * SR;
				s3 = s2 * SR;
				g = g2_func(xi, C, SR);
			} else {
				s2 = SL * SL;
				s3 = s2 * SL;
				g = g2_func(xi, C, SL);
			}
		} else {
			s1_2 = S1 * S1;
			s1_3 = s1_2 * S1;
			s2_2 = S2 * S2;
			s2_3 = s2_2 * S2;
			g1 = g2_func(xi, C, S1);
			g2 = g2_func(xi, C, S2);
		}

		double wi = gsl_vector_get(gf->_vw, i);

		switch (gf->_fit_type) {
		case GAUSSFIT_ACS:
			gsl_matrix_set(jacobian, i, 0, g * wi);
			gsl_matrix_set(jacobian, i, 1, A * g * d / s2 * wi);
			gsl_matrix_set(jacobian, i, 2, A * g * d2 / s3 * wi);
			break;
		case GAUSSFIT_OACS:
			gsl_matrix_set(jacobian, i, 0, 1 * wi);
			gsl_matrix_set(jacobian, i, 1, g * wi);
			gsl_matrix_set(jacobian, i, 2, A * g * d / s2 * wi);
			gsl_matrix_set(jacobian, i, 3, A * g * d2 / s3 * wi);
			break;
		case GAUSSFIT_OMACS:
			gsl_matrix_set(jacobian, i, 0, 1 * wi);
			gsl_matrix_set(jacobian, i, 1, xi * wi);
			gsl_matrix_set(jacobian, i, 2, g * wi);
			gsl_matrix_set(jacobian, i, 3, A * g * d / s2 * wi);
			gsl_matrix_set(jacobian, i, 4, A * g * d2 / s3 * wi);
			break;
		case GAUSSFIT_ASYM_ACS:
			if (d >= 0) {
				gsl_matrix_set(jacobian, i, 2, 0.0);
				gsl_matrix_set(jacobian, i, 3, A * g * d2 / s3 * wi);
			} else {
				gsl_matrix_set(jacobian, i, 2, A * g * d2 / s3 * wi);
				gsl_matrix_set(jacobian, i, 3, 0.0);
			}
			gsl_matrix_set(jacobian, i, 0, g * wi);
			gsl_matrix_set(jacobian, i, 1, A * g * d / s2 * wi);
			break;
		case GAUSSFIT_ASYM_OACS:
			if (d >= 0) {
				gsl_matrix_set(jacobian, i, 3, 0.0);
				gsl_matrix_set(jacobian, i, 4, A * g * d2 / s3 * wi);
			} else {
				gsl_matrix_set(jacobian, i, 3, A * g * d2 / s3 * wi);
				gsl_matrix_set(jacobian, i, 4, 0.0);
			}
			gsl_matrix_set(jacobian, i, 0, 1 * wi);
			gsl_matrix_set(jacobian, i, 1, g * wi);
			gsl_matrix_set(jacobian, i, 2, A * g * d / s2 * wi);
			break;
		case GAUSSFIT_ASYM_OMACS:
			if (d >= 0) {
				gsl_matrix_set(jacobian, i, 4, 0.0);
				gsl_matrix_set(jacobian, i, 5, A * g * d2 / s3 * wi);
			} else {
				gsl_matrix_set(jacobian, i, 4, A * g * d2 / s3 * wi);
				gsl_matrix_set(jacobian, i, 5, 0.0);
			}
			gsl_matrix_set(jacobian, i, 0, 1 * wi);
			gsl_matrix_set(jacobian, i, 1, xi * wi);
			gsl_matrix_set(jacobian, i, 2, g * wi);
			gsl_matrix_set(jacobian, i, 3, A * g * d / s2 * wi);
			break;
		case GAUSSFIT_DG_ACS:
			gsl_matrix_set(jacobian, i, 0, g1 * wi);
			gsl_matrix_set(jacobian, i, 1, g2 * wi);
			gsl_matrix_set(jacobian, i, 2,
					(A1 * g1 * d / s1_2 + A2 * g2 * d / s2_2) * wi);
			gsl_matrix_set(jacobian, i, 3, A1 * g1 * d2 / s1_3 * wi);
			gsl_matrix_set(jacobian, i, 4, A2 * g2 * d2 / s2_3 * wi);
			break;
		case GAUSSFIT_DG_OACS:
			gsl_matrix_set(jacobian, i, 0, 1 * wi);
			gsl_matrix_set(jacobian, i, 1, g1 * wi);
			gsl_matrix_set(jacobian, i, 2, g2 * wi);
			gsl_matrix_set(jacobian, i, 3,
					(A1 * g1 * d / s1_2 + A2 * g2 * d / s2_2) * wi);
			gsl_matrix_set(jacobian, i, 4, A1 * g1 * d2 / s1_3 * wi);
			gsl_matrix_set(jacobian, i, 5, A2 * g2 * d2 / s2_3 * wi);
			break;
		case GAUSSFIT_DG_OMACS:
			gsl_matrix_set(jacobian, i, 0, 1 * wi);
			gsl_matrix_set(jacobian, i, 1, xi * wi);
			gsl_matrix_set(jacobian, i, 2, g1 * wi);
			gsl_matrix_set(jacobian, i, 3, g2 * wi);
			gsl_matrix_set(jacobian, i, 4,
					(A1 * g1 * d / s1_2 + A2 * g2 * d / s2_2) * wi);
			gsl_matrix_set(jacobian, i, 5, A1 * g1 * d2 / s1_3 * wi);
			gsl_matrix_set(jacobian, i, 6, A2 * g2 * d2 / s2_3 * wi);
			break;
		default:
			break;
		}
	}

	return GSL_SUCCESS;
}

int GaussFit::gauss_fdf(const gsl_vector *coeffs, void *data, gsl_vector *f,
		gsl_matrix *J) {

	gauss_f(coeffs, data, f);
	gauss_df(coeffs, data, J);

	return GSL_SUCCESS;
}

void GaussFit::print_state(gsl_multifit_fdfsolver *s) {

	double O, M, A, A1, A2, C, S, SL, SR, S1, S2;

	if (_fit_type < GAUSSFIT_ASYM_ACS) {
		get_coeffs(s->x, &O, &M, &A, &C, &S);

		fprintf(stderr,
				"iter %2zu: O = %.4f, M = %.4f, A = %.4f, C = %.4f, S = %.4f, |f(x)| = %.4f\n",
				iterations, O, M, A, C, S, gsl_blas_dnrm2(s->f));
	} else if (_fit_type < GAUSSFIT_DG_ACS) {
		get_asym_coeffs(s->x, &O, &M, &A, &C, &SL, &SR);

		fprintf(stderr,
				"iter %2zu: O = %.4f, M = %.4f, A = %.4f, C = %.4f, S_L = %.4f, S_R = %.4f, |f(x)| = %.4f\n",
				iterations, O, M, A, C, SL, SR, gsl_blas_dnrm2(s->f));

	} else {
		get_dg_coeffs(s->x, &O, &M, &A1, &A2, &C, &S1, &S2);

		fprintf(stderr,
				"iter %2zu: O = %.4f, M = %.4f, A1 = %.4f, A2 = %.4f, C = %.4f, S_1 = %.4f, S_2 = %.4f, |f(x)| = %.4f\n",
				iterations, O, M, A1, A2, C, S1, S2, gsl_blas_dnrm2(s->f));

	}
}

void GaussFit::print_results() {
	printf("=============================\n");
	printf("status = %s\n", gsl_strerror(status));
	printf("number of iterations: %zu\n", iterations);
	printf("chisq/dof = %g\n", chisq / dof);
	printf("=============================\n");

	int i = 0;
	if (_fit_type == GAUSSFIT_OACS || _fit_type == GAUSSFIT_OMACS
			|| _fit_type == GAUSSFIT_ASYM_OACS
			|| _fit_type == GAUSSFIT_ASYM_OMACS || _fit_type == GAUSSFIT_DG_OACS
			|| _fit_type == GAUSSFIT_DG_OMACS) {
		printf("Offset    = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
	}
	if (_fit_type == GAUSSFIT_OMACS || _fit_type == GAUSSFIT_ASYM_OMACS
			|| _fit_type == GAUSSFIT_DG_OMACS) {
		printf("Slope       = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
	}
	if (_fit_type < GAUSSFIT_DG_ACS) {
		printf("Amplitude   = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
	} else {
		printf("Amplitude 1 = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
		printf("Amplitude 2 = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
	}
	printf("Center      = %10.3f +/- %10.3f\n", result[i], error[i]);
	i++;
	if (_fit_type < GAUSSFIT_ASYM_ACS) {
		printf("Sigma       = %10.3f +/- %10.3f\n", result[i], error[i]);
	} else if (_fit_type < GAUSSFIT_DG_ACS){
		printf("Sigma L     = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
		printf("Sigma R     = %10.3f +/- %10.3f\n", result[i], error[i]);
	} else {
		printf("Sigma 1     = %10.3f +/- %10.3f\n", result[i], error[i]);
		i++;
		printf("Sigma 2     = %10.3f +/- %10.3f\n", result[i], error[i]);
	}

}

bool GaussFit::get_init_values(double *init_values, gfit_type_t fit_type, double thr) {
	size_t y_min_ndx, y_max_ndx;
	double y_min, y_max;

	// Fitting with less than 10 points is not meaningful
	if (_n < 10 || init_values == NULL)
		return (false);

	// We take 1/10 of samples at either end of x and calculate the slope from the average
	size_t sn = _n/10;
	double ys = vector_mean(_vy, 0, sn);
	double ye = vector_mean(_vy, _n-sn, sn);
	double xs = vector_mean(_vx, 0, sn);
	double xe = vector_mean(_vx, _n-sn, sn);
	if (xs >= xe) {
		return (false);
	}
	double slope = (ye - ys) / (xe - xs);
	double offset = ys - slope * xs;

	gsl_vector_minmax_index(_vy, &y_min_ndx, &y_max_ndx);
	y_min = gsl_vector_get(_vy, y_min_ndx);
	y_max = gsl_vector_get(_vy, y_max_ndx);


	gsl_vector *tmp1= gsl_vector_alloc(_n);
	gsl_vector *tmp2= gsl_vector_alloc(_n);
	gsl_vector_memcpy(tmp1, _vy);
	gsl_vector_memcpy(tmp2, _vx);
	gsl_vector_scale(tmp2, slope);
	gsl_vector_add_constant(tmp2, offset);
	gsl_vector_sub(tmp1, tmp2);

	gsl_vector_minmax_index(tmp1, &y_min_ndx, &y_max_ndx);
	y_min = gsl_vector_get(tmp1, y_min_ndx);
	y_max = gsl_vector_get(tmp1, y_max_ndx);

	double amplitude = y_max - y_min;

	double ythr = amplitude * thr; // We only consider points in the [thr, 1.0] range for the calculation of the width and centre

	double norm = 0.0, x = 0.0, x2 = 0.0;

	for (size_t i = 0; i < _n; i++) {
		double xi = gsl_vector_get(_vx, i);
		double yi = gsl_vector_get(tmp1, i);

		// Need to filter the noise
		if (fabs(yi) > ythr) {
			norm += yi;
			x += (yi * xi);
			x2 += (yi * xi * xi);
		}
	}

	gsl_vector_free(tmp1);
	gsl_vector_free(tmp2);

	double mean = x / norm;
	double rms2 = x2 / norm - mean * mean;
	double rms = rms2 > 0. ? sqrt(rms2) : 1.;

	switch (fit_type) {
	case GAUSSFIT_ACS:
		init_values[0] = offset + amplitude;
		init_values[1] = mean;
		init_values[2] = rms;
		break;
	case GAUSSFIT_OACS:
		init_values[0] = offset;
		init_values[1] = amplitude;
		init_values[2] = mean;
		init_values[3] = rms;
		break;
	case GAUSSFIT_OMACS:
		init_values[0] = offset;
		init_values[1] = slope;
		init_values[2] = amplitude;
		init_values[3] = mean;
		init_values[4] = rms;
		break;
	case GAUSSFIT_ASYM_ACS:
		init_values[0] = offset + amplitude;
		init_values[1] = mean;
		init_values[2] = rms;
		init_values[3] = rms;
		break;
	case GAUSSFIT_ASYM_OACS:
		init_values[0] = offset;
		init_values[1] = amplitude;
		init_values[2] = mean;
		init_values[3] = rms;
		init_values[4] = rms;
		break;
	case GAUSSFIT_ASYM_OMACS:
		init_values[0] = offset;
		init_values[1] = slope;
		init_values[2] = amplitude;
		init_values[3] = mean;
		init_values[4] = rms;
		init_values[5] = rms;
		break;
	case GAUSSFIT_DG_ACS:
		init_values[0] = (offset + amplitude) / 2.0;
		init_values[1] = init_values[0];
		init_values[2] = mean;
		init_values[3] = rms / 2.0;
		init_values[4] = rms * 2.0;
		break;
	case GAUSSFIT_DG_OACS:
		init_values[0] = offset;
		init_values[1] = amplitude / 2.0;
		init_values[2] = init_values[1];
		init_values[3] = mean;
		init_values[4] = rms / 2.0;
		init_values[5] = rms * 2.0;
		break;
	case GAUSSFIT_DG_OMACS:
		init_values[0] = offset;
		init_values[1] = slope;
		init_values[2] = amplitude / 2.0;
		init_values[3] = init_values[2];
		init_values[4] = mean;
		init_values[5] = rms / 2.0;
		init_values[6] = rms * 2.0;
		break;
	default:
		return (false);
		break;
	}

	return (true);
}

int GaussFit::fit(double *init_values, gfit_type_t fit_type, double abs_tol,
		double rel_tol) {

	_fit_type = fit_type;

	switch (fit_type) {
	case GAUSSFIT_ACS:
		_p = 3;
		break;
	case GAUSSFIT_OACS:
		_p = 4;
		break;
	case GAUSSFIT_OMACS:
		_p = 5;
		break;
	case GAUSSFIT_ASYM_ACS:
		_p = 4;
		break;
	case GAUSSFIT_ASYM_OACS:
		_p = 5;
		break;
	case GAUSSFIT_ASYM_OMACS:
		_p = 6;
		break;
	case GAUSSFIT_DG_ACS:
		_p = 5;
		break;
	case GAUSSFIT_DG_OACS:
		_p = 6;
		break;
	case GAUSSFIT_DG_OMACS:
		_p = 7;
		break;
	default:
		_fit_type = GAUSSFIT_ACS;
		_p = 3;
		break;
	}

	if (_n < _p) {
	    printf("Not enough data points for a fit %d, minimum is %d\n", _n, _p);
	    return GSL_EINVAL;
	}

	dof = _n - _p;

	const gsl_multifit_fdfsolver_type *T = gsl_multifit_fdfsolver_lmsder;
	gsl_multifit_fdfsolver *solver = gsl_multifit_fdfsolver_alloc(T, _n, _p);

	double _init_v[10];
	double *coeffs_ptr;

	if (init_values != NULL) {
		coeffs_ptr = init_values;
	} else {
		get_init_values(_init_v, _fit_type);
		coeffs_ptr = _init_v;
	}

	gsl_vector_view coeffs = gsl_vector_view_array(coeffs_ptr, _p);
	gsl_matrix *covar = gsl_matrix_alloc(_p, _p);

	gsl_multifit_function_fdf fdf;
	fdf.f = &gauss_f;
	fdf.df = &gauss_df;
	fdf.fdf = &gauss_fdf;
	fdf.n = _n;
	fdf.p = _p;
	fdf.params = (void*) this;

	gsl_multifit_fdfsolver_set(solver, &fdf, &coeffs.vector);

	if (_verbose) {
		print_state(solver);
	}

	iterations = 0;
	do {
		iterations++;
		status = gsl_multifit_fdfsolver_iterate(solver);

		if (_verbose) {
			print_state(solver);
		}

		if (status && status != GSL_CONTINUE)
			break;

		status = gsl_multifit_test_delta(solver->dx, solver->x, abs_tol,
				rel_tol);
	} while (status == GSL_CONTINUE && iterations < MAX_ITERATIONS);

	gsl_multifit_covar(solver->J, 0.0, covar);

	gsl_blas_ddot(solver->f, solver->f, &chisq);

	double c = GSL_MAX_DBL(1, sqrt(chisq / dof));

#define FIT(i) gsl_vector_get(solver->x, i)
#define ERR(i) sqrt(gsl_matrix_get(covar,i,i))

	for (int i = 0; i < 10; i++) {
		if (i < _p) {
			result[i] = FIT(i);
			error[i] = c * ERR(i);
		} else {
			result[i] = 0;
			error[i] = 0;
		}
	}

	gsl_multifit_fdfsolver_free(solver);
	gsl_matrix_free(covar);

	return status;

}

} // namespace gauss_fit
