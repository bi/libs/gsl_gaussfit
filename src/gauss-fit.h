#ifndef __GAUSS_FIT_H__
#define __GAUSS_FIT_H__

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlin.h>


namespace gauss_fit {

extern const char *__GAUSSFIT_VERSION__;

typedef enum {
	COEFF_OFFSET,
	COEFF_SLOPE,
	COEFF_AMPLITUDE,
	COEFF_AMPLITUDE_1,
	COEFF_AMPLITUDE_2,
	COEFF_CENTRE,
	COEFF_SIGMA,
	COEFF_SIGMA_L,
	COEFF_SIGMA_R,
	COEFF_SIGMA_1,
	COEFF_SIGMA_2,
	COEFFICIENTS_MAX_SIZE
} coefficient_type_t;

/**
 * enum of the available fit functions.
 */
typedef enum {
	GAUSSFIT_ACS,			///< 0
	GAUSSFIT_OACS,			///< 1
	GAUSSFIT_OMACS,			///< 2
	GAUSSFIT_ASYM_ACS,		///< 3
	GAUSSFIT_ASYM_OACS,		///< 4
	GAUSSFIT_ASYM_OMACS,	///< 5
	GAUSSFIT_DG_ACS, 		///< 6
	GAUSSFIT_DG_OACS,		///< 6
	GAUSSFIT_DG_OMACS		///< 6
} gfit_type_t;

/**
 * Class for fitting data points using Gauss functions
 *
 * @author Enrico Bravin Enrico.Bravin@Cern.Ch
 * @version 1.1
 *
 * A class for performing Gaussian function fits.
 * The class supports several variants of the Gaussian function.
 *
 * The basic fit fuction is the 3 parameters Gaussian
 * \f[ g_3(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2} \f]
 *
 * It is also possible to fit asymmetric Gaussians where the sigma on the left and
 * right sides (around the centre) are different
 * \f[ \textrm{asym\_}g_4(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_\textrm{left}}\right)^2} \textrm{for } x < \mu \f]
 * \f[ \textrm{asym\_}g_4(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_\textrm{right}}\right)^2} \textrm{for } x \geq \mu \f]
 *
 * Or double concentric Gaussian
 * \f[ \textrm{dg\_}g_5(x)= A_1\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_1}\right)^2} + A_2\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_2}\right)^2}\f]
 *
 * To that, one can add a constant offset (p0) or a line offset (p1).
 */
class GaussFit {
public:

	/**
	 * Constructor:
	 * @param x array of x values
	 * @param y array of y values
	 * @param w array of weight values (should be 1.0/error of the y points).
	 *    If w is `null` default weights of 1.0 will be used. In this case the
	 *    chi square will not be meaningful.
	 * @param nb_of_points size of the `x`, `y` (and eventually `w`) arrays
	 */
	GaussFit(const double *x, const double *y, const double *w, size_t nb_of_points);
	~GaussFit();

	/**
	 * Fit routine: Call this function to perform the fit
	 *
	 * The data points should have been set previously either in the class constructor
	 * or using the {@link set_data} method.
	 *
	 * @param init_values array of double with the starting values for the minimization.
	 *  			The number and order of the parameters depend on the fit type.
	 *  			For Gaussian fits:
	 * 				<ul>
	 * 				<li>GAUSSFIT_ACS:   [Amplitude, Centre, Sigma]
	 * 				<li>GAUSSFIT_OACS:  [Offset, Amplitude, Centre, Sigma]
	 * 				<li>GAUSSFIT_OMACS: [Offset, Slope, Amplitude, Centre, Sigma]
	 * 				</ul>
	 * 				For asymmetric Gaussian fits:
	 * 				<ul>
	 * 				<li>GAUSSFIT_ASYM_ACS:   [Amplitude, Centre, Sigma_Left, Sigma_Right]
	 * 				<li>GAUSSFIT_ASYM_OACS:  [Offset, Amplitude, Centre, Sigma_Left, Sigma_Right]
	 * 				<li>GAUSSFIT_ASYM_OMACS: [Offset, Slope, Amplitude, Centre, Sigma_Left, Sigma_Right]
	 * 				</ul>
	 * 				For double Gaussian fits:
	 * 				<ul>
	 * 				<li>GAUSSFIT_DG_ACS:   [Amplitude1, Amplitude2, Centre, Sigma1, Sigma2]
	 * 				<li>GAUSSFIT_DG_OACS:  [Offset, Amplitude1, Amplitude2, Centre, Sigma1, Sigma2]
	 * 				<li>GAUSSFIT_DG_OMACS: [Offset, Slope, Amplitude1, Amplitude2, Centre, Sigma1, Sigma2]
	 * 				</ul>
	 * 				If <code>init_values</code> is <code>null</code> the function
	 * 				will try to calculate appropriate start values.
	 * @param fit_type indicates the type of fit to perform, possible functions are:
	 * 				<ul>
	 * 			 	<li>GAUSSFIT_ACS:   \f$ g_3(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}\f$
	 * 			 	<li>GAUSSFIT_OACS:  \f$ g_4(x)= q + g_3(x)\f$
	 * 			 	<li>GAUSSFIT_OMACS: \f$ g_5(x)= q + m \cdot x + g_3(x)\f$
	 * 			 	<li>GAUSSFIT_ASYM_ACS:
	 * 			 		<ul>
	 * 			 		<li>\f$ \textrm{asym\_}g_4(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_\textrm{left}}\right)^2} \f$ for \f$x < \mu \f$
	 * 			 		<li>\f$ \textrm{asym\_}g_4(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_\textrm{right}}\right)^2} \f$ for \f$x \geq \mu \f$
	 * 			 		</ul>
	 * 			 	<li>GAUSSFIT_ASYM_ACS: \f$ \textrm{asym\_}g_5(x)= q + \textrm{asym\_}g_4(x) \f$
	 * 			 	<li>GAUSSFIT_ASYM_ACS: \f$ \textrm{asym\_}g_6(x)= q + m\cdot x + \textrm{asym\_}g_4(x) \f$
	 * 			 	<li>GAUSSFIT_DG_ACS:   \f$ \textrm{dg\_}g_5(x)= A_1\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_1}\right)^2} + A_2\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_2}\right)^2}\f$
	 * 			 	<li>GAUSSFIT_DG_OACS:  \f$ \textrm{dg\_}g_6(x)= q + \textrm{dg\_}g_5(x)\f$
	 * 			 	<li>GAUSSFIT_DG_OMACS: \f$ \textrm{dg\_}g_7(x)= q + m \cdot x + \textrm{dg\_}g_5(x)\f$
	 *
	 * 			 	</ul>
	 * 			 	With \f$q\f$= Offset, \f$m\f$= Slope, \f$A\f$= Amplitude, \f$\mu\f$= Centre and \f$ \sigma \f$ = Sigma
	 *
	 * @param abs_tol Absolute tolerance
	 * @param rel_tol Relative tolerance
	 * @return The status of the fit solver at the end of the fit iteration.
	 * 				 More details can be found in the documentation of the
	 * 				 gsl_multifit_fdfsolver_iterate and gsl_multifit_test_delta
	 * 				 functions of the gsl library.
	 *
	 * The minimization will stop when \f$ |dx|< (\textrm{abs\_tol} + |x_i|\cdot\textrm{rel\_tol}) \f$.
	 * <ul>
	 * <li>\f$ x_i \f$ is the vector with the fit parameters for the i_th iteration.
	 * <li> \f$ dx= |x_i-x_{i-1}| \f$ is the delta between to iterations.
	 * </ul>
	 */
	int fit(double *init_values, gfit_type_t fit_type = GAUSSFIT_OMACS,
			double abs_tol = 1e-4, double rel_tol = 1e-4);

	/**
	 * Calculates the initial values for a g5 Gaussian using the MIN, MAX, MEAN and RMS values of the data points.
	 *
	 * @param init_values pointer to an array of at least 5 elements. The calculated initial values will be stored here.
	 * <ul>
	 * <li> Offset 		MIN(y)
	 * <li> Slope  		(y[n-1]-y[0])/(x[n-1]-x[0])
	 * <li> Amplitude 	MAX(y)-MIN(y)
	 * <li> Centre		AVE(x*y) of points with y > MIN(y)+0.2*Amplitude
	 * <li> Sigma		RMS of points with y > MIN(y)+0.2*Amplitude
	 * </ul>
	 * @param fit_type the gfit_type_t value indicating the type of fit that will be initialized.
	 * @param thr the threshold applied in eliminating the noise, defaults to 0.2 (20%).
	 * The parameters and their positions are the same as in {@link fit}.
	 * @return true if the values in init_values are valid, false otherwise
	 *
	 */
	bool get_init_values(double *init_values, gfit_type_t fit_type, double thr = 0.2);

	/**
	 * Replaces the data arrays with new values
	 * @param x array of x values
	 * @param y array of y values
	 * @param w array of weight values (should be 1.0/error of the y points)
	 *    if w is NULL default weights of 1.0 will be used. In this case the
	 *    chi square will not be correct.
	 * @param nb_of_points size of the x, y (and eventually w) arrays
	 *
	 */
	void set_data(const double *x, const double *y, const double *w, size_t nb_of_points);

	/**
	 * Helper function that facilitates extracting a given coefficient from an array
	 * since the position of the coefficients depend on the fit type
	 * Returns the requested coefficient from the coefficients array. If the requested coefficient is not valid
	 * for the fit type zero is returned.
	 *
	 * @param coefficients the array holding the coefficients. If NULL the fit result array will be used.
	 * @param which identifier of the requested coefficient.
	 * @return the fit coefficient value if applicable for the fitted function, zero otherwise.
	 */
	double get_coefficient(double *coefficients, coefficient_type_t which);

	/**
	 * Prints out the summary and results of the fit
	 */
	void print_results();

	/**
	 * Basic Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param mu the value indicating the position of the centre of the Gauss function.
	 * @param sigma the value indicating the width of the Gauss function.
	 * @return the value of the Gauss function \f$ f(x)= e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}\f$.
	 */
	static double g2_func(double x, double mu, double sigma);

	/**
	 * Scaled Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param amp the value indicating the amplitude of the Gauss function A.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma the value indicating the width of the Gauss function \f$\sigma\f$.
	 * @return the value of the Gauss function \f$ f(x)= A \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}\f$.
	 */
	static double g3_func(double x, double amp, double mu, double sigma);

	/**
	 * Constant offset and scaled Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param offset the value of the constant offset of the Gauss function q.
	 * @param amp the value indicating the amplitude of the Gauss function A.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma the value indicating the width of the Gauss function \f$\sigma\f$.
	 * @return the value of the Gauss function \f$ f(x)= q + A \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}\f$.
	 */
	static double g4_func(double x, double offset, double amp, double mu,
			double sigma);

	/**
	 * Linear offset and scaled Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param offset the value of the constant offset of the Gauss function q.
	 * @param slope the value of the linear offset of the Gauss function m.
	 * @param amp the value indicating the amplitude of the Gauss function A.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma the value indicating the width of the Gauss function \f$\sigma\f$.
	 * @return the value of the Gauss function \f$ f(x)= q + m\cdot x + A \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2}\f$.
	 */
	static double g5_func(double x, double offset, double slope, double amp,
			double mu, double sigma);

	/**
	 * Scaled double Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.

	 * @param amp1 the value indicating the amplitude of the first Gauss function A1.
	 * @param amp2 the value indicating the amplitude of the second Gauss function A2.
	 * @param mu the value indicating the position of the centre of the Gauss functions \f$\mu\f$.
	 * @param sigma1 the value indicating the width of the first Gauss function \f$\sigma_1\f$.
	 * @param sigma2 the value indicating the width of the second Gauss function \f$\sigma_2\f$.
	 * @return the value of the double Gauss function \f$ f(x)= A_1 \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_1}\right)^2} + A_2 \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_2}\right)^2}\f$.
	 */
	static double dg_g5_func(double x, double amp1, double amp2, double mu,
			double sigma1, double sigma2);

	/**
	 * Constant offset and scaled double Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param offset the value of the constant offset of the Gauss function q.
	 * @param amp1 the value indicating the amplitude of the first Gauss function A1.
	 * @param amp2 the value indicating the amplitude of the second Gauss function A2.
	 * @param mu the value indicating the position of the centre of the Gauss functions \f$\mu\f$.
	 * @param sigma1 the value indicating the width of the first Gauss function \f$\sigma_1\f$.
	 * @param sigma2 the value indicating the width of the second Gauss function \f$\sigma_2\f$.
	 * @return the value of the double Gauss function \f$ f(x)= q + A_1 \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_1}\right)^2} + A_2 \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_2}\right)^2}\f$.
	 */
	static double dg_g6_func(double x, double offset, double amp1, double amp2,
			double mu, double sigma1, double sigma2);

	/**
	 * Linear offset and scaled double Gaussian function
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param offset the value of the constant offset of the Gauss function q.
	 * @param slope the value of the linear offset of the Gauss function m.
	 * @param amp1 the value indicating the amplitude of the first Gauss function A1.
	 * @param amp2 the value indicating the amplitude of the second Gauss function A2.
	 * @param mu the value indicating the position of the centre of the Gauss functions \f$\mu\f$.
	 * @param sigma1 the value indicating the width of the first Gauss function \f$\sigma_1\f$.
	 * @param sigma2 the value indicating the width of the second Gauss function \f$\sigma_2\f$.
	 * @return the value of the double Gauss function \f$ f(x)= q + m\cdot x + A_1 \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_1}\right)^2} + A_2 \cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_2}\right)^2}\f$.
	 */
	static double dg_g7_func(double x, double offset, double slope, double amp1,
			double amp2, double mu, double sigma1, double sigma2);

	/**
	 * Basic asymmetric Gaussian function. Same as {@link g2_func} but asymmetric
	 *
	 * The asymmetric Gauss function has different values of \f$\sigma\f$ for \f$x<\mu\f$ and \f$x\geq\mu\f$
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma_l the value indicating the width of the Gauss function  \f$\sigma_L\f$for \f$x<\mu\f$.
	 * @param sigma_r the value indicating the width of the Gauss function  \f$\sigma_R\f$for \f$x\geq\mu\f$.
	 * @return the value of the Gauss function \f$ f(x)= e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_{[L,R]}}\right)^2}\f$.
	 */
	static double asym_g3_func(double x, double mu, double sigma_l,
			double sigma_r);

	/**
	 * Scaled asymmetric Gaussian function. Same as {@link g3_func} but asymmetric
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param amp the value indicating the amplitude of the Gauss function A.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma_l the value indicating the width of the Gauss function  \f$\sigma_L\f$for \f$x<\mu\f$.
	 * @param sigma_r the value indicating the width of the Gauss function  \f$\sigma_R\f$for \f$x\geq\mu\f$.
	 * @return the value of the Gauss function \f$ f(x)= A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_{[L,R]}}\right)^2}\f$.
	 */
	static double asym_g4_func(double x, double amp, double mu, double sigma_l,
			double sigma_r);
	/**
	 * Constant offset and scaled asymmetric Gaussian function. Same as {@link g4_func} but asymmetric
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param offset the value of the constant offset of the Gauss function q.
	 * @param amp the value indicating the amplitude of the Gauss function A.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma_l the value indicating the width of the Gauss function  \f$\sigma_L\f$for \f$x<\mu\f$.
	 * @param sigma_r the value indicating the width of the Gauss function  \f$\sigma_R\f$for \f$x\geq\mu\f$.
	 * @return the value of the Gauss function \f$ f(x)= q + A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_{[L,R]}}\right)^2}\f$.
	 */
	static double asym_g5_func(double x, double offset, double amp, double mu,
			double sigma_l, double sigma_r);
	/**
	 * Linear offset and scaled asymmetric Gaussian function. Same as {@link g5_func} but asymmetric
	 *
	 * @param x the value at which the function should be evaluated.
	 * @param offset the value of the constant offset of the Gauss function q.
	 * @param slope the value of the linear offset of the Gauss function m.
	 * @param amp the value indicating the amplitude of the Gauss function A.
	 * @param mu the value indicating the position of the centre of the Gauss function \f$\mu\f$.
	 * @param sigma_l the value indicating the width of the Gauss function  \f$\sigma_L\f$for \f$x<\mu\f$.
	 * @param sigma_r the value indicating the width of the Gauss function  \f$\sigma_R\f$for \f$x\geq\mu\f$.
	 * @return the value of the Gauss function \f$ f(x)= q + m\cdot x + A\cdot e^{-\frac{1}{2}\left(\frac{x-\mu}{\sigma_{[L,R]}}\right)^2}\f$.
	 */
	static double asym_g6_func(double x, double offset, double slope,
			double amp, double mu, double sigma_l, double sigma_r);

	/**
	 * If verbose is set to true the status of the fit is printed at each iteration
	 */
	void set_verbose(bool verbose) {
		_verbose = verbose;
	}
	;

	/**
	 * Returns the verbose status
	 */
	bool get_verbose() {
		return _verbose;
	}
	;

	/**
	 * Returns the number of elements of the data arrays
	 */
	size_t get_n() {
		return _n;
	}

	/**
	 * Returns the number of free parameters used for the fit. The value only makes sense after {@link fit} has been called.
	 * The value depends on the fitting function selected.
	 */
	size_t get_p() {
		return _p;
	}

	/**
	 * Returns the last status of the fit process
	 * see gls_errno.h for the details
	 */
	int get_status() {
		return status;
	}

	/**
	 * Returns the fitting function used in the fit. Possible values are:
	 * <ul>
	 * <li> GAUSSFIT_ACS
	 * <li> GAUSSFIT_OACS
	 * <li> GAUSSFIT_OMACS
	 * <li> GAUSSFIT_ASYM_ACS
	 * <li> GAUSSFIT_ASYM_OACS
	 * <li> GAUSSFIT_ASYM_OMACS
	 * </ul>
	 */
	gfit_type_t get_fit_type() {
		return _fit_type;
	}

	double result[10]; ///< Array containing the results of the fit. Only the first {@link get_p} values are meaningful.
	double error[10]; ///< Array containing the error (covariance) matrix of the fit. Only the first {@link get_p} values are meaningful.
	double chisq; 		///< The \f$ \chi^2 \f$ value of the fit.
	int iterations; 	///< The number of iterations of the last fit process.
	double dof;	///< The number of degrees of freedom of the problem ({@link get_n}-{@link get_p}).
	int status;			///< The exit status of the last fit process.

private:

	void get_coeffs(const gsl_vector * coeffs, double *O, double *M, double *A,
			double *C, double *S);
	void get_asym_coeffs(const gsl_vector * coeffs, double *O, double *M,
			double *A, double *C, double *Sl, double *SR);
	void get_dg_coeffs(const gsl_vector * coeffs, double *O, double *M,
			double *A1, double *A2, double *C, double *S1, double *S2);
	void print_state(gsl_multifit_fdfsolver * s);

	static int gauss_f(const gsl_vector * coeffs, void *data,
			gsl_vector * residuals);
	static int gauss_df(const gsl_vector * coeffs, void *data,
			gsl_matrix * jacobian);
	static int gauss_fdf(const gsl_vector * x, void *data, gsl_vector *f,
			gsl_matrix *J);

	size_t _n;
	size_t _p;
	gsl_vector *_vx;
	gsl_vector *_vy;
	gsl_vector *_vw;
	bool _verbose;
	gfit_type_t _fit_type;
};

} // namespace gauss_fit

#endif // __GAUSS_FIT_H__
