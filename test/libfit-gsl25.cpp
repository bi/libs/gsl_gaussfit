#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit_nlinear.h>

#define N      1000    /* number of data points to fit */

enum {GAUSSFIT_ACS, GAUSSFIT_OACS, GAUSSFIT_OMACS};

struct data_points {
  size_t n;
  double *x;
  double *y;
};

void *memdup(void *src, size_t len) {
	void *ptr;
	ptr= malloc(len);
	memcpy(ptr, src, len);
	return ptr;
}

class GaussFit {
public:
	GaussFit(double *x, double *y, double *w, size_t nb_of_points, int fit_type);
	~GaussFit();
	
	int fit(double *init_values, double *final_values);
	
	static double g2_func(double x, double mu, double sigma);
	static double g3_func(double x, double amp, double mu, double sigma);
	static double g4_func(double x, double offset, double amp, double mu, double sigma);
	static double g5_func(double x, double offset, double slope, double amp, double mu, double sigma);
	
	static int gauss_f (const gsl_vector * coeffs, void *data, gsl_vector * residuals);
	static int gauss_df (const gsl_vector * coeffs, void *data, gsl_matrix * jacobian);
	static void callback(const size_t iter, void *params, const gsl_multifit_nlinear_workspace *w);
	
	size_t _n;
	double *_x;
	double *_y;
	double *_w;
	int fit_type;
};

GaussFit::GaussFit(double *x, double *y, double *w, size_t nb_of_points, int fit_type):
		fit_type(fit_type), _n(nb_of_points), _x(NULL), _y(NULL), _w(NULL) {
	_x= (double*) memdup((void*) x, sizeof(double)*_n);
	_y= (double*) memdup((void*) y, sizeof(double)*_n);
	if (w == NULL) {
		_w= (double*) malloc(sizeof(double)*_n);
		for (size_t i=0;i<_n;i++) {
			_w[i]= 1.0;
		}
	} else {
		_w= (double*) memdup((void*) w, sizeof(double)*_n);
	}
}

GaussFit::~GaussFit() {
	_n= 0;
	free(_x);
	free(_y);
	free(_w);
}

inline double GaussFit::g2_func(double x, double mu, double sigma){
	double k;
	double f;
	
	k= (x-mu)/sigma;
	f= exp(-0.5*k*k);
	
	return (f);
}

inline double GaussFit::g3_func(double x, double amp, double mu, double sigma) {
	
	return (amp * g2_func(x, mu, sigma));
}

inline double GaussFit::g4_func(double x, double offset, double amp, double mu, double sigma) {
	
	return (offset + g3_func(x, amp, mu, sigma));
}

inline double GaussFit::g5_func(double x, double offset, double slope, double amp, double mu, double sigma) {
	
	return (slope*x + g4_func(x, offset, amp, mu, sigma));
}


int GaussFit::gauss_f (const gsl_vector * coeffs, void *data, gsl_vector * residuals)
{
	GaussFit *gf= (GaussFit*) data;
		
	double O = gsl_vector_get (coeffs, 0);
	double M = gsl_vector_get (coeffs, 1);  
	double A = gsl_vector_get (coeffs, 2);
	double C = gsl_vector_get (coeffs, 3);
	double S = gsl_vector_get (coeffs, 4);
	
	size_t i;
	
	for (i = 0; i < gf->_n; i++)
	{
	  /* Model Yi = A * exp(-lambda * t_i) + b */
	  double Yi = g5_func(gf->_x[i], O, M, A, C, S);
	  gsl_vector_set (residuals, i, Yi - gf->_y[i]);
	}
	
	return GSL_SUCCESS;
}

int GaussFit::gauss_df (const gsl_vector * coeffs, void *data, gsl_matrix * jacobian)
{
	GaussFit *gf= (GaussFit*) data;

	double O = gsl_vector_get (coeffs, 0);
	double M = gsl_vector_get (coeffs, 1);  
	double A = gsl_vector_get (coeffs, 2);
	double C = gsl_vector_get (coeffs, 3);
	double S = gsl_vector_get (coeffs, 4);
	
	printf("O= %.3f, M= %.3f, A= %.3f, C= %.3f, S= %.3f\n", O, M, A, C, S);
	size_t i;
	
	for (i = 0; i < gf->_n; i++)
	{
	  /* Jacobian matrix J(i,j) = dfi / dxj,             */
	  /* where fi = (Yi - yi)/sigma[i],                  */
	  /*       Yi = O + x*M + A * exp(-1/2*((x-C)/S)^2)  */
	  double xi= gf->_x[i];
	  double g = g2_func(xi, C, S);
	  double d= xi-C;
	  double s2= S*S;
	  gsl_matrix_set (jacobian, i, 0, 1);
	  gsl_matrix_set (jacobian, i, 1, xi);
	  gsl_matrix_set (jacobian, i, 2, g);
	  gsl_matrix_set (jacobian, i, 3, A*g*d/s2);
	  gsl_matrix_set (jacobian, i, 4, A*g*d*d/(s2*S));
	}
	
	return GSL_SUCCESS;
}

void GaussFit::callback(const size_t iter, void *params, const gsl_multifit_nlinear_workspace *w)
{
  gsl_vector *residuals = gsl_multifit_nlinear_residual(w);
  gsl_vector *coeffs = gsl_multifit_nlinear_position(w);
  double rcond;

  /* compute reciprocal condition number of J(x) */
  gsl_multifit_nlinear_rcond(&rcond, w);

  fprintf(stderr, "iter %2zu: O = %.4f, M = %.4f, A = %.4f, C = %.4f, S = %.4f, cond(J) = %8.4f, |f(x)| = %.4f\n",
          iter,
          gsl_vector_get(coeffs, 0),
          gsl_vector_get(coeffs, 1),
          gsl_vector_get(coeffs, 2),
          gsl_vector_get(coeffs, 3),
          gsl_vector_get(coeffs, 4),
          1.0 / rcond,
          gsl_blas_dnrm2(residuals));
}

int GaussFit::fit(double *init_values, double *final_values) {
	
	const gsl_multifit_nlinear_type *T = gsl_multifit_nlinear_trust;
	gsl_multifit_nlinear_workspace *ws;
	gsl_multifit_nlinear_fdf fdf;
	gsl_multifit_nlinear_parameters fdf_params= gsl_multifit_nlinear_default_parameters();
	const size_t p = 5;
	
	gsl_vector *residuals;
	gsl_matrix *jacobian;
	gsl_matrix *covar = gsl_matrix_alloc (p, p);
	gsl_vector_view coeff = gsl_vector_view_array (init_values, p);
	gsl_vector_view wts = gsl_vector_view_array(_w, _n);
	double chisq, chisq0;
	int status, info;
	size_t i;
	
	const double xtol = 1e-8;
	const double gtol = 1e-8;
	const double ftol = 0.0;
	
	
	/* define the function to be minimized */
	fdf.f = gauss_f;
	fdf.df = gauss_df;   /* set to NULL for finite-difference Jacobian */
	fdf.fvv = NULL;     /* not using geodesic acceleration */
	fdf.n = _n;
	fdf.p = p;
	fdf.params = (void*) this;
	

	/* allocate workspace with default parameters */
	ws = gsl_multifit_nlinear_alloc (T, &fdf_params, _n, p);
	
	/* initialize solver with starting point and weights */
	gsl_multifit_nlinear_winit (&coeff.vector, &wts.vector, &fdf, ws);
	
	/* compute initial cost function */
	residuals = gsl_multifit_nlinear_residual(ws);
	gsl_blas_ddot(residuals, residuals, &chisq0);
	
	/* solve the system with a maximum of 100 iterations */
	status = gsl_multifit_nlinear_driver(100, xtol, gtol, ftol,
									   callback, NULL, &info, ws);
	
	/* compute covariance of best fit parameters */
	jacobian = gsl_multifit_nlinear_jac(ws);
	gsl_multifit_nlinear_covar (jacobian, 0.0, covar);
	
	/* compute final cost */
	gsl_blas_ddot(residuals, residuals, &chisq);
	
	
	for(int i=0;i<5;i++) {
	  final_values[i]= gsl_vector_get(ws->x, i);
	}
	
	fprintf(stderr, "summary from method '%s/%s'\n",
		  gsl_multifit_nlinear_name(ws),
		  gsl_multifit_nlinear_trs_name(ws));
	fprintf(stderr, "number of iterations: %zu\n",
		  gsl_multifit_nlinear_niter(ws));
	fprintf(stderr, "function evaluations: %zu\n", fdf.nevalf);
	fprintf(stderr, "Jacobian evaluations: %zu\n", fdf.nevaldf);
	fprintf(stderr, "reason for stopping: %s\n",
		  (info == 1) ? "small step size" : "small gradient");
	fprintf(stderr, "initial |f(x)| = %f\n", sqrt(chisq0));
	fprintf(stderr, "final   |f(x)| = %f\n", sqrt(chisq));

    double dof = _n - p;
    double c = GSL_MAX_DBL(1, sqrt(chisq / dof));

    fprintf(stderr, "chisq/dof = %g\n", chisq / dof);
    i= 0;
    fprintf (stderr, "Offset    = %.5f +/- %.5f\n", final_values[0], c*sqrt(gsl_matrix_get(covar,0,0)));
    fprintf (stderr, "Slope     = %.5f +/- %.5f\n", final_values[1], c*sqrt(gsl_matrix_get(covar,1,1)));
    fprintf (stderr, "Amplitude = %.5f +/- %.5f\n", final_values[2], c*sqrt(gsl_matrix_get(covar,2,2)));
    fprintf (stderr, "Center    = %.5f +/- %.5f\n", final_values[3], c*sqrt(gsl_matrix_get(covar,3,3)));
    fprintf (stderr, "Sigma     = %.5f +/- %.5f\n", final_values[4], c*sqrt(gsl_matrix_get(covar,4,4)));
    i= 0;
    fprintf (stderr, "g= [%.4f, %.4f, %.4f, %.4f, %.4f]\n", final_values[0], final_values[1],
    		final_values[2], final_values[3], final_values[4]);
 

    fprintf (stderr, "status = %s\n", gsl_strerror (status));

	gsl_multifit_nlinear_free (ws);
	gsl_matrix_free (covar);

}

int main (void)
{

	double x[N];
	double y[N];
	double w[N];
	double noise= 500.0;
	double weight= 1.0/(noise*noise);
	
	gsl_rng * r;
	gsl_rng_env_setup();
	r = gsl_rng_alloc(gsl_rng_default);
	
	/* this is the data to be fitted */
	for (size_t i = 0; i < N; i++)
	{
		x[i] = -5.0 + i *  5.0 / (N/2);
		y[i] = GaussFit::g5_func(x[i], 0.2, 0.01, 10000, 0.7, 0.8) + gsl_ran_gaussian(r, noise);
		w[i] = weight;
	  printf ("data: %g %g\n", x[i], y[i]);
	}
	
	gsl_rng_free (r);


	GaussFit *gfit= new GaussFit(x, y, w, N, GAUSSFIT_OMACS);
	double start[5]= {0.0, 0.0, 15000.0, 0.0, 1.0};
	double end[5];
	
	gfit->fit(start, end);
	
}
