#include <stdio.h>
#include <time.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gauss-fit.h>

#define N      1000    /* number of data points to fit */

#define M      10      /* number of cycles */

int main(void) {

	double x[N];
	double y[N];
	double w[N];
	double noise;
	double weight;
	double offset;
	double slope;
	double ampl, ampl1, ampl2;
	double centre;
	double sigma, sigma1, sigma2;
	double error, error1, error2;

	gsl_rng * r;
	gsl_rng_env_setup();
	r = gsl_rng_alloc(gsl_rng_default);
	gsl_rng_set(r, time(NULL));

	printf("Lib version %s\n",gauss_fit::__GAUSSFIT_VERSION__);
	
	for (int l = 0; l < M; l++) {
		ampl = gsl_ran_flat(r, 500.0, 5000.0);
		noise = gsl_ran_flat(r, 0.0, 0.05);
		weight = 1.0 / (noise * ampl);
		offset = ampl * gsl_ran_flat(r, -0.1, 0.1);
		slope = gsl_ran_flat(r, -1.0, 1.0);
		centre = gsl_ran_flat(r, -2.0, 2.0);
		sigma = gsl_ran_flat(r, 0.2, 2.0);

		/* this is the data to be fitted */
		for (size_t i = 0; i < N; i++) {
			x[i] = -5.0 + i * 10.0 / N;
			y[i] = gauss_fit::GaussFit::g5_func(x[i], offset, slope, ampl,
					centre, sigma) + ampl * gsl_ran_gaussian(r, noise);
			w[i] = weight;
		}

		gauss_fit::GaussFit *gfit = new gauss_fit::GaussFit(x, y, w, N);
//		double start[5] =
//				{ 0.0, 0.0, ampl * gsl_ran_flat(r, 0.5, 1.5), 0.0, 5.0 };
		double start[10];
		gfit->get_init_values(start, gauss_fit::GAUSSFIT_OMACS);
		gfit->fit(start, gauss_fit::GAUSSFIT_OMACS, 1e-4, 1e-6);

		printf("\n");
		//gfit->print_results();

		error = (sigma - fabs(gfit->result[4])) / sigma * 100;
		printf(
				"Amp= %.3f Noise%%= %.1f chisq/dof= %.3f status= %d iterations= %d error= %.3f\n",
				ampl, noise * 100, gfit->chisq / gfit->dof, gfit->status,
				gfit->iterations, error);
		printf("true= [%12.4f, %12.4f, %12.4f, %12.4f, %12.4f]\n", offset,
				slope, ampl, centre, sigma);

		printf("start = [%12.4f, %12.4f, %12.4f, %12.4f, %12.4f]\n", start[0],
				start[1], start[2], start[3], start[4]);
		printf("fit = [%12.4f, %12.4f, %12.4f, %12.4f, %12.4f]\n",
				gfit->result[0], gfit->result[1], gfit->result[2],
				gfit->result[3], fabs(gfit->result[4]));
	}

	for (int l = 0; l < M; l++) {
		ampl1 = gsl_ran_flat(r, 500.0, 5000.0);
		noise = gsl_ran_flat(r, 0.0, 0.05);
		weight = 1.0 / (noise * ampl1);
		offset = ampl1 * gsl_ran_flat(r, -0.1, 0.1);
		slope = gsl_ran_flat(r, -1.0, 1.0);
		centre = gsl_ran_flat(r, -2.0, 2.0);
		sigma1 = gsl_ran_flat(r, 0.2, 2.0);
		sigma2 = sigma1 * gsl_ran_flat(r, 1.8, 2.2);
		ampl2 = ampl1 * sigma1 / sigma2;

		/* this is the data to be fitted */
		for (size_t i = 0; i < N; i++) {
			x[i] = -5.0 + i * 10.0 / N;
			y[i] = gauss_fit::GaussFit::dg_g7_func(x[i], offset, slope, ampl1,
					ampl2, centre, sigma1, sigma2)
					+ ampl * gsl_ran_gaussian(r, noise);
			w[i] = weight;
		}

		gauss_fit::GaussFit *gfit = new gauss_fit::GaussFit(x, y, w, N);
//		double start[5] =
//				{ 0.0, 0.0, ampl * gsl_ran_flat(r, 0.5, 1.5), 0.0, 5.0 };
		double start[10];
		gfit->get_init_values(start, gauss_fit::GAUSSFIT_DG_OMACS);
		gfit->fit(start, gauss_fit::GAUSSFIT_DG_OMACS, 1e-4, 1e-6);

		printf("\n");
		//gfit->print_results();

		gfit->result[5] = fabs(gfit->result[5]);
		gfit->result[6] = fabs(gfit->result[6]);

		double s1, s2, a1, a2;
		if (gfit->result[5] < gfit->result[6] && sigma1 < sigma2) {
			a1 = gfit->result[2];
			a2 = gfit->result[3];
			s1 = gfit->result[5];
			s2 = gfit->result[6];
		} else {
			a1 = gfit->result[3];
			a2 = gfit->result[2];
			s1 = gfit->result[6];
			s2 = gfit->result[5];
		}

		error1 = (sigma1 - s1) / sigma1 * 100;
		error2 = (sigma2 - s2) / sigma2 * 100;
		printf(
				"Amp1= %.3f Amp2= %.3f Noise%%= %.1f chisq/dof= %.3f status= %d iterations= %d error1 = %.3f%% error2 = %.3f%%\n",
				ampl1, ampl2, noise * 100, gfit->chisq / gfit->dof,
				gfit->status, gfit->iterations, error1, error2);
		printf(
				"true= [%12.4f, %12.4f, %12.4f, %12.4f, %12.4f, %12.4f, %12.4f]\n",
				offset, slope, ampl1, ampl2, centre, sigma1, sigma2);

		printf(
				"start = [%12.4f, %12.4f, %12.4f, %12.4f, %12.4f, %12.4f, %12.4f]\n",
				start[0], start[1], start[2], start[3], start[4], start[5],
				start[6]);
		printf(
				"fit = [%12.4f, %12.4f, %12.4f, %12.4f, %12.4f, %12.4f, %12.4f]\n",
				gfit->result[0], gfit->result[1], a1, a2, gfit->result[4], s1,
				s2);
	}

	gsl_rng_free(r);

}
